package se331.lab.rest.entity;

import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Student {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @ToString.Exclude
    Long id;
    @ToString.Exclude
    String studentId;
    @ToString.Exclude
    String name;
    @ToString.Exclude
    String surname;
    @ToString.Exclude
    Double gpa;
    @ToString.Exclude
    String image;
    @ToString.Exclude
    Integer penAmount;
    @ToString.Exclude
    String description;
    @ManyToOne
    @ToString.Exclude
    Lecturer advisor;
    @ManyToMany(mappedBy = "students")
    @Builder.Default
    @ToString.Exclude
    List<Course> enrolledCourses = new ArrayList<>();


}
