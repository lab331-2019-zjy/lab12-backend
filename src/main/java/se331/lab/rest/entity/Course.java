package se331.lab.rest.entity;

import com.fasterxml.jackson.annotation.JsonBackReference;
import lombok.*;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@Entity
@AllArgsConstructor
@NoArgsConstructor
public class Course {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @ToString.Exclude
    Long id;
    @ToString.Exclude
    String courseId;
    @ToString.Exclude
    String courseName;
    @ToString.Exclude
    String content;
    @ManyToOne
    @JsonBackReference
    @ToString.Exclude
    Lecturer lecturer;
    @ManyToMany
    @Builder.Default
    @ToString.Exclude
    List<Student> students = new ArrayList<>();
}
