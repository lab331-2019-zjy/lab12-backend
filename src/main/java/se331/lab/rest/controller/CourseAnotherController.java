package se331.lab.rest.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import se331.lab.rest.entity.Course;
import se331.lab.rest.entity.Student;
import se331.lab.rest.mapper.MapperUtil;
import se331.lab.rest.service.CourseAnotherService;
import se331.lab.rest.service.CourseService;

@Controller
public class CourseAnotherController {
    @Autowired
    CourseAnotherService courseAnotherService;
    @Autowired
    CourseService courserService;

    @GetMapping("course/amount/{numOfStudent}")
    public ResponseEntity<?> getCourseWithAmountOfStudent(@PathVariable Integer numOfStudent){
        return ResponseEntity.ok(MapperUtil.INSTANCE.getCourseDto(courseAnotherService.getCourseWhichStudentEnrolledMoreThan(numOfStudent)));
    }
    @GetMapping("courses/{courseId}")
    public ResponseEntity<?> getCourseById(@PathVariable String courseId){
        return ResponseEntity.ok(MapperUtil.INSTANCE.getCourseDto(courserService.getCourse(courseId)));
}

    @PostMapping("/courses")
    public ResponseEntity saveCourse(@RequestBody Course course) {
        return ResponseEntity.ok(courseAnotherService.saveCourse(course));
    }
}
