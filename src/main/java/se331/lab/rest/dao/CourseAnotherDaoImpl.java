package se331.lab.rest.dao;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import se331.lab.rest.entity.Course;
import se331.lab.rest.repository.CourseRepository;

import java.util.List;

@Slf4j
@Repository
public class CourseAnotherDaoImpl implements CourseAnotherDao {
    @Autowired
    CourseRepository courseRepository;
    @Override
    public List<Course> findAll() {
        return courseRepository.findAll();
    }

    @Override
    public Course getCourseById(String courseId) {
        log.info(courseId);
        return  courseRepository.findByCourseId(courseId);
    }

    @Override
    public Course saveCourse(Course course) {
        log.info(course.getCourseId());
        return courseRepository.save(course);
    }
}
