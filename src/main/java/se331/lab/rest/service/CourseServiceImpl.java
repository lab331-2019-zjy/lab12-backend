package se331.lab.rest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import se331.lab.rest.dao.CourseAnotherDao;
import se331.lab.rest.dao.CourseDao;
import se331.lab.rest.entity.Course;

import java.util.List;
@Service
public class CourseServiceImpl implements CourseService {
    @Autowired
    CourseDao courseDao;
    @Autowired
    CourseAnotherDao courseAnotherDao;
    @Override
    public List<Course> getAllCourses() {
        return courseDao.getAllCourses();
    }
    @Override
    public  Course getCourse(String courseId){
        return courseAnotherDao.getCourseById(courseId);
    }
}
